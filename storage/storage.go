package storage

import (
	"bootcamp/article/models"
	"errors"
	"strings"
)

var ErrorNotFound = errors.New("not found")
var ErrorAlreadyExists = errors.New("already exists")
var ErrorInGetAll = errors.New("get all throws an error")

type ArticleStorage map[int]models.Article

func (storage ArticleStorage) Add(entity models.Article) error {
	if _, ok := storage[entity.ID]; ok {
		return ErrorAlreadyExists
	}
	storage[entity.ID] = entity
	return nil
}

func (storage ArticleStorage) GetByID(ID int) (models.Article, error) {
	var resp models.Article
	if val, ok := storage[ID]; ok {
		resp = val
		return resp, nil
	}

	return resp, ErrorNotFound
}

func (storage ArticleStorage) GetAll() []models.Article {
	var resp []models.Article
	for _, val := range storage {
		resp = append(resp, val)
	}

	return resp
}

func (storage ArticleStorage) Search(str string) []models.Article {
	var resp []models.Article
	for _, entity := range storage {
		body := (strings.Join(strings.Split(strings.TrimSpace(entity.Body), ","), " "))
		searchExp, strExp := strings.ToLower(body), strings.ToLower(strings.TrimSpace(str))
		searchByName := strings.ToLower(strings.TrimSpace(entity.Author.Firstname))
		if strings.EqualFold(entity.Title, str) {
			resp = append(resp, entity)
			return resp
		} else if strings.Contains(searchExp, strExp) {
			resp = append(resp, entity)
			return resp
		} else if strings.Contains(searchByName, strExp) {
			resp = append(resp, entity)
			return resp
		}
	}

	return resp
}

func (storage ArticleStorage) Update(entity models.Article) error {
	_, err := storage[entity.ID]
	if !err {
		return ErrorNotFound
	}
	storage[entity.ID] = entity

	return nil
}

func (storage ArticleStorage) Delete(ID int) error {
	val, err := storage[ID]
	if !err {
		return ErrorNotFound
	}
	delete(storage, val.ID)

	return nil
}
