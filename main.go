package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"time"
)

var inMemory storage.ArticleStorage

func main() {
	inMemory = make(storage.ArticleStorage)
	var a1 models.Article
	a1.ID = 1
	a1.Title = "Lorem"
	a1.Body = "Lorem ipsum, random text for Frontend"
	var p models.Person = models.Person{
		Firstname: "John",
		Lastname:  "Doe",
	}
	a1.Author = p
	t := time.Now()
	a1.CreatedAt = &t
	err := inMemory.Add(a1)
	if err != nil {
		fmt.Println("27th line", err)
	}
	err = inMemory.Add(a1)
	if err != nil {
		fmt.Println("31th line", err)
	}
	// fmt.Println("33th line", inMemory)

	var a2 models.Article
	a2.ID = 2
	a2.Title = "Golang"
	a2.Body = "Go is cool language, and it is Golang"
	var p2 models.Person = models.Person{
		Firstname: "Ryuk ",
		Lastname:  "Kira",
	}
	a2.Author = p2
	t2 := time.Now()
	a2.CreatedAt = &t2

	err = inMemory.Add(a2)
	if err != nil {
		fmt.Println("49th line", err)
	}
	fmt.Println("51th line", inMemory)

	id2, err := inMemory.GetByID(2)
	if err != nil {
		fmt.Println("55th line", err)
	}
	fmt.Println("57th line", id2)

	_, err = inMemory.GetByID(3)
	if err != nil {
		fmt.Println("61th line | ", err)
	}
	// fmt.Println("63th line | ", inMemory)

	all := inMemory.GetAll()
	fmt.Println("66th line | ", inMemory)
	fmt.Println("67th line | ", all)

	found := inMemory.Search("Golang")
	fmt.Println("70th line | ", found)

	foundAgain := inMemory.Search("golang")
	fmt.Println("73th line | ", foundAgain)

	notFound := inMemory.Search("Goolang")
	fmt.Println("76th line | ", notFound)

	mustFound := inMemory.Search("Go")
	fmt.Println("79th line | ", mustFound)

	willFound := inMemory.Search("     fRontend       ")
	fmt.Println("82th line | ", willFound)

	searchByName := inMemory.Search(" rYuk ")
	fmt.Println("85th line | ", searchByName)

	var updatedData models.Article
	updatedData.ID = 2
	updatedData.Body = "Hello World from JavaScript"
	updatedData.Title = "JS is complicated"
	var pForUpdated models.Person = models.Person{
		Firstname: "Light",
		Lastname:  "Yagami",
	}
	updatedData.Author = pForUpdated
	updatedTime := time.Now()
	updatedData.CreatedAt = &updatedTime
	updated := inMemory.Update(updatedData)
	fmt.Println("99th line | ", updated)
	fmt.Println("100th line | ", inMemory)

	deleted := inMemory.Delete(1)
	fmt.Println("103th line |", deleted)
	fmt.Println("104th line | ", inMemory)

	byId, err := inMemory.GetByID(2)
	if err != nil {
		fmt.Println("108th line | ", err)
	}
	fmt.Println("110th line | ", byId)

	deletedId, err := inMemory.GetByID(1)
	if err != nil {
		fmt.Println("114th line | ", err)
	}
	fmt.Println("116th line | ", deletedId)
	fmt.Println("117th line | final map ", inMemory)
}
