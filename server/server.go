package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"strconv"

	_ "bootcamp/article/server/docs"

	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"       // swagger embed files
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
)

var inMemory storage.ArticleStorage

// @title Swagger Example API
// @version 1.1
// @description This is a article CRUD API documentation.
// @termsOfService https://udevs.io
// @contact.name Ryuk Kira
// @contact.url https://udevs.io
// @contact.email tursunov_khudoyberdi@mail.ru
// @host localhost:8080
// @Accept json
// @Produce json
func main() {

	gin.SetMode(gin.DebugMode)
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	inMemory = make(storage.ArticleStorage)

	r.POST("/articles", CreateHandler)
	r.GET("/articles", GetAllHandler)
	r.GET("/articles/:id", GetByIDHandler)
	r.PUT("/articles/:id", UpdateHandler)
	r.DELETE("/articles/:id", DeleteHandler)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":8080") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

type DefaultError struct {
	Message string `json:"message"`
}

type ErrorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type SuccessResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// CreateHandler godoc
// @Router /articles [POST]
// @Summary Create an article
// @Description Used to create article based on input data
// @ID Create-Handler
// @Param data body models.Article true "Article data"
// @Success 200 {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func CreateHandler(c *gin.Context) {
	var a1 models.Article
	err := c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	err = inMemory.Add(a1)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, SuccessResponse{
		Message: "ok",
	})
}

// GetAllHandler godoc
// @Router /articles [GET]
// @Summary get all articles
// @Description Used to retrieve all articles from memory or search based on pattern
// @ID Get-All-Handler
// @Param query query models.Article true "Existing articles"
// @Success 200 {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func GetAllHandler(c *gin.Context) {
	str := c.Query("body")
	fmt.Println(str)
	if len(str) == 0 {
		resp := inMemory.GetAll()

		c.JSON(200, SuccessResponse{
			Message: "ok",
			Data:    resp,
		})
		return
	}
	resp := inMemory.Search(str)
	c.JSON(200, SuccessResponse{
		Message: "ok",
		Data:    resp,
	})
}

// GetByIDHandler godoc
// @Router /articles/{id} [GET]
// @Summary Get one article by ID
// @Description Used to get article using its ID
// @ID Get-By-ID-Handler
// @Param id path int true "Article ID"
// @Success 200 {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func GetByIDHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	fmt.Println(idNum)
	resp, err := inMemory.GetByID(int(idNum))
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, SuccessResponse{
		Message: "ok",
		Data:    resp,
	})
}

// UpdateHandler godoc
// @Router /articles/{id} [PUT]
// @Summary Update particular article
// @Description Used to update any article related to specified ID in params.
// @ID Update-Handler
// @Param id path int true "Article ID"
// @Param data body models.Article true "Updating data"
// @Success 200 {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func UpdateHandler(c *gin.Context) {
	id := c.Param("id")
	idNum, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	var structId models.Article
	err = c.BindJSON(&structId)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
	}
	if structId.ID != idNum {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	err = inMemory.Update(structId)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, SuccessResponse{
		Message: "Updated your desired article!",
	})
}

// DeleteHandler godoc
// @Router /articles/{id} [DELETE]
// @Summary Delete an article
// @Description Used to delete articles using it's ID
// @ID Delete-Handler
// @Param id path int true "Article ID"
// @Success 200 {object} SuccessResponse
// @Failure 400,404 {object} ErrorResponse
// @Failure default {object} DefaultError
func DeleteHandler(c *gin.Context) {
	id := c.Param("id")
	idNum, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
	}
	fmt.Println(idNum)
	err = inMemory.Delete(idNum)
	if err != nil {
		c.JSON(400, ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}
	c.JSON(200, SuccessResponse{
		Message: "articles was successfully destroyed",
	})
}
