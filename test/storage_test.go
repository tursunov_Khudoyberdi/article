package test

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"testing"
	"time"
)

func add(a models.Article) error {
	return inMemory.Add(a)
}

func getByID(ID int) (models.Article, error) {
	return inMemory.GetByID(ID)
}

func getAll() []models.Article {
	return inMemory.GetAll()
}

func count() int {
	return len(inMemory)
}

func search(str string) []models.Article {
	return inMemory.Search(str)
}

func update(entity models.Article) error {
	return inMemory.Update(entity)
}

func deleteArticle(ID int) error {
	return inMemory.Delete(ID)
}

func TestStorage(t *testing.T) {
	t1 := time.Now()
	a1 := models.Article{
		ID: 1,
		Content: models.Content{
			Title: "Lorem",
			Body:  "Lorem ipsum",
		},
		Author: models.Person{
			Firstname: "John",
			Lastname:  "Doe",
		},
		CreatedAt: &t1,
	}

	t2 := time.Now()
	a2 := models.Article{
		ID: 7,
		Content: models.Content{
			Title: "CSS",
			Body:  "Cascading style sheet",
		},
		Author: models.Person{
			Firstname: "Jack",
			Lastname:  "Flord",
		},
		CreatedAt: &t2,
	}

	//// Positive test case
	c := count()
	err := add(a1)
	if err != nil {
		t.Errorf("Add FAILED: %v", err)
	}
	if count() != c+1 {
		t.Errorf("Add FAILED: expected count %d but got %d", c+1, count())
	}

	//// Negative test case
	err = add(a1)
	if err != storage.ErrorAlreadyExists {
		t.Error("Add FAILED: should return error", storage.ErrorAlreadyExists)
	}
	////

	t.Log("Add PASSED")

	res1, err := getByID(a1.ID)
	if err != nil {
		t.Errorf("GetByID FAILED: %v", err)
	}
	if a1 != res1 {
		t.Errorf("Add or GetByID FAILED: expected %v but got %v", a1, res1)
	}

	_, err = getByID(-1)
	if err != storage.ErrorNotFound {
		t.Errorf("Add returns error: %v", err)
	}

	t.Log("GetByID PASSED")

	counts := getAll()
	comCount := count()
	if len(counts) != comCount {
		t.Errorf("GetAll FAILED: expected %v but got %v", comCount, len(counts))
	}

	t.Log("GetAll PASSED")

	found := search("lOrEM")
	if found[0] != a1 {
		t.Errorf("search FAILED: expected %v but got %v", found[0], a1)
	}

	found1 := search("   jOhn   ")
	if found1[0] != a1 {
		t.Errorf("search FAILED: expected %v but got %v", found1[0], a1)
	}

	f := search("  hye  ")
	if len(f) > 0 {
		t.Errorf("search FAILED: search should return nil")
	}

	t.Log("Search PASSED")

	err = update(a2)
	if err != storage.ErrorNotFound {
		t.Errorf("update FAILED: %v", err)
	}

	// noErr = getByID(1)
	// try without using getByID

	t.Log("Update PASSED")

	err3 := deleteArticle(1)
	if err3 == storage.ErrorNotFound {
		t.Errorf("delete FAILED: %v", err3)
	}
	// expect error

	t.Log("Delete PASSED")
}
